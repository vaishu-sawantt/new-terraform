provider "aws" {
    region = "us-east-2"
    shared_config_files = ["/root/.aws/config"]
    shared_credentials_files = ["/root/.aws/credentials"]

  
}

resource "aws_instance" "my_instance" {
    ami = "ami-0b8b44ec9a8f90422"
    instance_type = "t2.micro"
    key_name = "gauri_1"
  
}